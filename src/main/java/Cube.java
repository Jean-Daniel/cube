import java.util.Scanner;

public class Cube
{
	public static final int MAX = 4098;
	public static final int BITS = 24;
	private String[] table;
	
	public Cube (String[] table)
	{
		this.table = table;
	}
	
	private String calcule(String op1, String op2)
	{
		while (op1.length() < op2.length()) {
			op1 = "0" + op1;
		}
		while (op2.length() < op1.length()) {
			op2 = "0" + op2;
		}
		String res = new String();
		for (int i=0; i < op1.length(); i++) {
			int bit1 = new Integer("" + op1.charAt(i)).intValue(); 
			int bit2 = new Integer("" + op2.charAt(i)).intValue(); 
			boolean resultat = false;
			if (bit1 == 0 && bit2 == 0) {
				resultat = false;
			}
			else if (bit1 == 0 && bit2 == 1) {
				resultat = false;
			}
			else if (bit1 == 1 && bit2 == 0) {
				resultat = false;
			}
			else if (bit1 == 1 && bit2 == 1) {
				resultat = true;
			}
			if (!resultat) {
				res += "0";
			}
			else {
				res += "1";
			}
		}
		return res;
	}
	
	private boolean containsOnlyZero(String s)
	{
		for (int i=0; i < s.length(); i++) {
			if (s.charAt(i) != '0') {
				return false;
			}
		}
		return true;
	}
	
	public void simplifier()
	{
		int i = 0;
		int new_i = 0;
	    String op1 = table[i];

	    while (!containsOnlyZero(op1)) {
	        String op2;
	        int j = i + 1;
	        op2 = table[j];
	        new_i = i + 1;
	        while (!containsOnlyZero(op2)) {
	        	String a = calcule(op1, op2);
	            if (a.equals(op1)) {
	                decaler(i);
	                new_i = i;
	                break;
	
	            }
	            if (a.equals(op2)) {
	                decaler(j);
	            } else {
	                j++;
	            }
	            op2 = table[j];
	        }
	
	        i = new_i;
	        op1 = table[i];	
	    }
	}

	public void decaler (int k)
	{
	    String a = table[k+1];
	    while (!containsOnlyZero(table[k])){
	        table[k] = a;
	        k++;
	        a = table[k+1];
	    }
	}
	
	void afficherCompte (String entete)
	{
		System.out.println(entete);
		int compte = 0;
	    for (int i=0; (i < table.length && !containsOnlyZero(table[i])); i++) {
	        compte++;
	    }
	    System.out.println("Total: " + compte + " lignes\n");
	}

	void afficherTable (String entete)
	{
		System.out.println(entete);
	    for (int i=0; (i < table.length && !containsOnlyZero(table[i])); i++) {
	        System.out.println(table[i]);
	    }
	    System.out.println();
	}

	public static void main(String[] args)
	{
        /*
        System.out.println("Press [enter] to continue");
        Scanner scan = new Scanner(System.in);
        scan.nextLine();
        System.out.println("Starting!");
        */
		
        String[] table = new String[MAX];
		table[MAX-1] = "0";
		table[MAX-2] = "0";
		for (int i=0; i<MAX-2; i++) {
			int x = (int)(Math.random() * (Math.pow(2, BITS) - 1));
			table[i] = Integer.toBinaryString(x);
		}
		Cube cube = new Cube(table);

		//cube.afficherTable("Table initiale");
	    cube.afficherCompte("Table initiale");

	    long temps = System.currentTimeMillis();
	    cube.simplifier();
	    temps = System.currentTimeMillis() - temps;

	    //cube.afficherTable("Table finale");
	    cube.afficherCompte("Table finale");

	    System.out.println("Temps algo : " + temps + " ms");
	}
}
